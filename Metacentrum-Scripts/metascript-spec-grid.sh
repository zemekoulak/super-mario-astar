#!/bin/bash

# seq -> FIRST INCREMENT LAST

for ss in 1 2 3 4 5 6 7 8 9 10 20
do
  for ttfw in {0.2..2.00..0.2}
  do

    sed -i "s/SS=[0-9]\+\.[0-9]\+/SS=$ss/" script-spec-grid.sh;
    sed -i "s/TTFW=[0-9]\+\.[0-9]\+/TTFW=$ttfw/" script-spec-grid.sh;
    #qsub script-spec-grid.sh;
    cat script-spec-grid.sh;

  done
done
